using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

public class DisplayBoundKeys : MonoBehaviour
{
    [SerializeField] InputActionAsset actionAsset;
    [SerializeField] TextMeshProUGUI jumpText;
    //[SerializeField] TextMeshProUGUI upText;
    [SerializeField] TextMeshProUGUI downText;
    [SerializeField] TextMeshProUGUI leftText;
    [SerializeField] TextMeshProUGUI rightText;

    private void Update()
    {
        getCurrentControls();
    }

    private void getCurrentControls()
    {
        jumpText.text = actionAsset.FindAction("Player/Jump").controls[0].displayName.ToUpper();

        InputControl[] moveTmp = actionAsset.FindAction("Player/Move").controls.ToArray();
        //upText.text = moveTmp[0].displayName.ToUpper();
        downText.text = moveTmp[1].displayName.ToUpper();
        leftText.text = moveTmp[2].displayName.ToUpper();
        rightText.text = moveTmp[3].displayName.ToUpper();
    }
}
