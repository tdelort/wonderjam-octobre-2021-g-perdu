using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flash : MonoBehaviour
{

    [SerializeField] private Material flashMaterial;
    [SerializeField] private float interval;
    [SerializeField] private int numberOfRepetitions;

    private SpriteRenderer spriteRenderer;
    private Material baseMaterial;

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        baseMaterial = spriteRenderer.material;
    }

    public void StartFlash()
    {
        StartCoroutine(FlashCoroutine());
    }

    private IEnumerator FlashCoroutine()
    {
        int nb = 0;
        while(nb < numberOfRepetitions)
        {
            nb++;
            spriteRenderer.material = flashMaterial;
            yield return new WaitForSeconds(interval);
            spriteRenderer.material = baseMaterial;
            yield return new WaitForSeconds(interval);
        }
    }
}
