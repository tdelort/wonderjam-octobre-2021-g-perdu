using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    [SerializeField] ParticleSystem dustCloud;
    [SerializeField] GameObject pauseCanvas;
    [SerializeField] float maxSpeed = 7.0f;
    [SerializeField] int jumpHeight = 2;
    [SerializeField] float frictionValue = 1.8f;
    [SerializeField] Animator animator;
    [SerializeField] AudioClip[] soundEffects;

    private Rigidbody rb;
    private Vector3 movementVector = Vector3.zero;
    private Flash flashComponent;
    private bool wantToJump = false;
    private bool canMove = false;
    private bool canJump = false;
    private int strength = 5;
    public bool winAnimation = false;

    /*void Start()
    {
        animator.SetBool("Direction", true);
        animator.SetBool("Jump", false);
        animator.SetBool("Death", false);
        animator.SetBool("Win", false);
        dustCloud.Stop();
        rb = GetComponent<Rigidbody>();
        flashComponent = GetComponent<Flash>();
    }

    private void Update()
    {
    }

    private void slowCharacter()
    {
        float sens = rb.velocity.x / Mathf.Abs(rb.velocity.x);
        rb.AddForce(-sens * Vector2.right * frictionValue * maxSpeed);
        if (Mathf.Abs(rb.velocity.x) <= 0.2f) { rb.velocity = new Vector2(0.0f, rb.velocity.y); }
    }
    */
    private void FixedUpdate()
    {
        //slowCharacter();
        if (movementVector == Vector3.zero)
        {
            Debug.Log("move");
            rb.AddForce(movementVector * maxSpeed);
            rb.velocity = new Vector3(Mathf.Clamp(rb.velocity.x, -maxSpeed, maxSpeed), Mathf.Clamp(rb.velocity.y, -maxSpeed, maxSpeed), Mathf.Clamp(rb.velocity.z, -maxSpeed, maxSpeed));
        }
    }

    private void OnMove(InputValue movementValue)
    {

        if (strength > 0)
        {
            Debug.Log("input");
            movementVector = movementValue.Get<Vector3>();
        }

    }
    /*
    private void OnJump()
    {
        wantToJump = true;
    }

    private void OnTriggerEnter(Collider collision)
    {
    }

    private void OnTriggerExit(Collider collision)
    {
    }

    public void TakeAHit(int damages, Vector3 direction)
    {
        if (strength > 0)
        {
            flashComponent.StartFlash();
            PushBack(direction);
            strength -= damages;
            if (strength <= 0)
            {
                StartCoroutine(DeathAnimation());
                return;
            }
        }

    }
    public void PushBack(Vector3 direction)
    {
        rb.velocity = new Vector3(0.0f, 0.0f, 0.0f);
        rb.AddForce(direction.normalized * rb.mass / 2.0f * Mathf.Sqrt(80.0f * 9.81f), ForceMode.Impulse);
    }

    IEnumerator DeathAnimation()
    {
        animator.SetBool("Death", true);
        strength = 0;
        yield return new WaitForSeconds(1);
    }

    public void OnPause()
    {
        if (Time.timeScale == 0)
        {
            pauseCanvas.SetActive(false);
            Time.timeScale = 1;
        }
        else
        {
            pauseCanvas.SetActive(true);
            Time.timeScale = 0;
        }
    }
    */
}
